import React, { useEffect, useState, useRef } from "react";
import { addDoc, collection, onSnapshot } from 'firebase/firestore';

// const [title, setTitle] = useState('')


const Slides = ({ database }) => {

    const [title, setTitle] = useState('')
    const [slidesData, setSlidesData] = useState([]);

    const isMounted = useRef()

    useEffect(() => {
        if (isMounted.current) {
            return
        }

        isMounted.current = true;
        getData()
    }, [])

    const collectionRef = collection(database, 'slidesData')

    const addData = () => {
        addDoc(collectionRef, {
            title: title
        })
            .then(() => {
                alert('Data insertion successful')
            })
            .catch(() => {
                alert('Data insertion failed')
            })
    }

    const getData = () => {
        onSnapshot(collectionRef, (data) => {
            setSlidesData(data.docs.map((doc) => {
                return { ...doc.data(), id: doc.id }
            }))
        })
    }

    return (
        <div className='container-slides'>
            <h1>Mes slides</h1>
            <input
                placeholder='Ajouter un titre'
                className='title-input'
                onChange={(event) => setTitle(event.target.value)}
                value={title}
            />
            <button
                className='add-slide'
                onClick={addData}
            >
                Créer une slide
            </button>

            <div className="slides-list">
                {slidesData.map((doc) => {
                    return (
                        <div className="slide">
                            <p>{doc.title}</p>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default Slides;